<?php
 class User extends Controller {
   //  public function index() {
   //    $data["judul"] = "User";
   //    $this->view("templates/header", $data);
   //    $this->view("user/index");
   //    $this->view("templates/footer");
   //  }

    public function index($nama = "dam", $pekerjaan = "pelajar") {
      $data["judul"] = "User";
      $data["nama"] = $nama;
      $data["pekerjaan"] = $pekerjaan;
      $this->view("templates/header", $data);
      $this->view("user/profile", $data);
      $this->view("templates/footer");
    }

    public function signup(){
      // var_dump($_POST);
      $data["user"] = $this->model("User_model")->getSignUp($_POST);
    }

    public function signin() {
      $row = $this->model("User_model")->getSignIn($_POST);
      if(count($row) === 1){
        // echo "<sqript>Login berhasil</sqript>";
        $BASE_URL = BASE_URL;
        header("Location: $BASE_URL/home/index");

      }else{
        // echo "<sqript>Login gagal</sqript>";
        // $this->view('user/login');
        header("Location: login");
        exit;
      }
    }

    public function register() {
      $data["judul"]="Register";
      $this->view("templates/header");
      $this->view("user/register");
    }

    public function login() {
      $data["judul"]="Login";
      $this->view("templates/header");
      $this->view("user/login");
    }
} 
?>
